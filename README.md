# split
Utility to split and merge files.

## Installation
1. `git clone https://gitlab.com/sarthakgaur/split`
2. `cd split`
2. `cargo build --release`
3. The executable is located in `target/release`

## Features
1. Split a file into multiple part files.
2. Size of a part file can be specified in `kb`, `mb` or `gb`.
3. Merge the part files into one file.
4. While merging any part file can be specified.

## Command Line Arguments
    USAGE:
        split [OPTIONS]

    OPTIONS:
        -d, --dir <DIR>                The directory where part files will be placed.
        -h, --help                     Print help information
        -m, --merge <FILE>             Name of a part file.
        -p, --part-size <part-size>    Max size of the part files. Eg. 10kb, 2mb, 1gb.
        -s, --split <FILE>             Name of the file to split.
        -V, --version                  Print version information


## Important
1. The part files are pure content files. No metadata is added to part files.
2. Use a program like `sha256sum` to check the checksum. 

## Example Usage
### Split Example Usage
`split -s image.jpg -p 300kb -d part_images`

This command will split `image.jpg` to many part files of size upto 300 Kilobytes in the `part_images` directory.

### Merge Example Usage
`split -m image.jpg.5`

This command will find and merge all the part files. It's not necessary to specify the first part file. Any part file can be specified here. The program will find the first part file and start the merge process from there.
