use fehler::throws;
use std::cmp;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::PathBuf;

#[derive(Debug)]
pub struct PartFileWriter {
    remaining_space: usize,
    buffer: BufWriter<File>,
}

impl PartFileWriter {
    #[throws(anyhow::Error)]
    pub fn new(path: PathBuf, max_size: usize) -> PartFileWriter {
        let file = File::create(&path)?;
        let remaining_space = max_size;
        let buffer = BufWriter::new(file);

        PartFileWriter {
            remaining_space,
            buffer,
        }
    }

    pub fn is_full(&self) -> bool {
        self.remaining_space == 0
    }

    #[throws(anyhow::Error)]
    pub fn write(&mut self, bytes: &[u8]) -> usize {
        let min = cmp::min(self.remaining_space as usize, bytes.len());
        let bytes_to_write = &bytes[..min];
        let bytes_written = self.buffer.write(bytes_to_write)?;
        self.remaining_space -= bytes_written;

        bytes_written
    }

    #[throws(anyhow::Error)]
    pub fn close(&mut self) {
        self.buffer.flush()?
    }
}
