mod command;
mod merger;
mod part_file_writer;
mod request;
mod splitter;

use fehler::throws;
use request::Request;

#[throws(anyhow::Error)]
fn main() {
    let command = command::get_command();
    let matches = command.get_matches();
    let request = Request::new(&matches)?;

    request.handle()?;
}
