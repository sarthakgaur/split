use crate::merger::Merger;
use crate::splitter::Splitter;
use clap::ArgMatches;
use fehler::throws;
use std::fs;
use std::path::{Path, PathBuf};

#[derive(Debug)]
pub enum Request {
    SplitFile(SplitRequest),
    MergeFile(MergeRequest),
}

impl Request {
    #[throws(anyhow::Error)]
    pub fn new(matches: &ArgMatches) -> Request {
        let request = if matches.is_present("split") {
            Request::SplitFile(SplitRequest::new(matches)?)
        } else if matches.is_present("merge") {
            Request::MergeFile(MergeRequest::new(matches)?)
        } else {
            panic!("No valid command given.")
        };

        request
    }

    #[throws(anyhow::Error)]
    pub fn handle(self) {
        match self {
            Request::SplitFile(split_request) => split_request.handle()?,
            Request::MergeFile(merge_request) => merge_request.handle()?,
        }
    }
}

#[derive(Debug)]
pub struct SplitRequest {
    file_path: PathBuf,
    part_size: usize,
    parts_dir: PathBuf,
}

impl SplitRequest {
    #[throws(anyhow::Error)]
    pub fn new(matches: &ArgMatches) -> SplitRequest {
        let file_path = matches.value_of("split").unwrap();
        let part_size = matches.value_of("part-size").unwrap();
        let parts_dir = matches.value_of("dir").unwrap();

        let file_path = Path::new(file_path).to_path_buf();
        let part_size = SplitRequest::parse_part_size(part_size)?;
        let parts_dir = Path::new(parts_dir).to_path_buf();

        SplitRequest {
            file_path,
            part_size,
            parts_dir,
        }
    }

    #[throws(anyhow::Error)]
    fn handle(self) {
        let file_path = self.file_path;
        let part_size = self.part_size;
        let parts_dir = self.parts_dir;

        fs::create_dir_all(&parts_dir)?;

        let mut splitter = Splitter::new(&file_path, part_size, parts_dir)?;
        splitter.split()?;
    }

    #[throws(anyhow::Error)]
    fn parse_part_size(part_size: &str) -> usize {
        let len = part_size.len();

        if len < 3 {
            panic!("Invalid part-size.")
        }

        let size = &part_size[..len - 2];
        let unit = &part_size[len - 2..];

        SplitRequest::to_bytes(size, unit)?
    }

    #[throws(anyhow::Error)]
    fn to_bytes(size: &str, unit: &str) -> usize {
        let num = size.parse::<usize>()?;
        let unit = unit.to_lowercase();
        let base: usize = 1024;

        match unit.as_str() {
            "kb" => num * base.pow(1),
            "mb" => num * base.pow(2),
            "gb" => num * base.pow(3),
            _ => panic!("Invalid unit."),
        }
    }
}

#[derive(Debug)]
pub struct MergeRequest {
    part_file_path: PathBuf,
}

impl MergeRequest {
    #[throws(anyhow::Error)]
    pub fn new(matches: &ArgMatches) -> MergeRequest {
        let part_file_path = matches.value_of("merge").unwrap();
        let part_file_path = Path::new(part_file_path).to_path_buf();

        MergeRequest { part_file_path }
    }

    #[throws(anyhow::Error)]
    fn handle(self) {
        let part_file_path = self.part_file_path;
        let mut merger = Merger::new(&part_file_path)?;

        merger.merge()?;
    }
}
