use clap::{Arg, Command};

pub fn get_command() -> Command<'static> {
    Command::new("split")
        .version("0.1")
        .about("Utility to split and merge files.")
        .arg(
            Arg::new("split")
                .short('s')
                .long("split")
                .value_name("FILE")
                .takes_value(true)
                .requires_all(&["part-size", "dir"])
                .help("Name of the file to split."),
        )
        .arg(
            Arg::new("part-size")
                .short('p')
                .long("part-size")
                .takes_value(true)
                .requires("split")
                .help("Max size of the part files. Eg. 10kb, 2mb, 1gb."),
        )
        .arg(
            Arg::new("dir")
                .short('d')
                .long("dir")
                .value_name("DIR")
                .takes_value(true)
                .requires("split")
                .help("The directory where part files will be placed."),
        )
        .arg(
            Arg::new("merge")
                .short('m')
                .long("merge")
                .value_name("FILE")
                .takes_value(true)
                .conflicts_with("split")
                .help("Name of a part file."),
        )
}
