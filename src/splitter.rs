use crate::part_file_writer::PartFileWriter;
use fehler::throws;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::{Path, PathBuf};

const BUFFER_SIZE: usize = 8192;

#[derive(Debug)]
pub struct Splitter {
    file_name: String,
    part_size: usize,
    parts_dir: PathBuf,
    bytes_remaining: usize,
    part_count: usize,
    reader: BufReader<File>,
    writer: Option<PartFileWriter>,
    buffer: [u8; BUFFER_SIZE],
    buffer_start: usize,
    buffer_end: usize,
}

impl Splitter {
    #[throws(anyhow::Error)]
    pub fn new(file_path: &Path, part_size: usize, parts_dir: PathBuf) -> Splitter {
        let file = File::open(&file_path)?;
        let file_name = file_path.file_name().unwrap().to_str().unwrap().to_owned();
        let file_size = file.metadata()?.len();
        let bytes_remaining = file_size as usize;
        let part_count = 0;
        let reader = BufReader::new(file);
        let writer = None;
        let buffer = [0; BUFFER_SIZE];
        let buffer_start = 0;
        let buffer_end = 0;

        Splitter {
            file_name,
            part_size,
            parts_dir,
            bytes_remaining,
            part_count,
            reader,
            writer,
            buffer,
            buffer_start,
            buffer_end,
        }
    }

    #[throws(anyhow::Error)]
    pub fn split(&mut self) {
        while self.bytes_remaining > 0 {
            self.set_buffer()?;
            self.set_writer()?;
            self.write_buffer()?;
        }
    }

    #[throws(anyhow::Error)]
    fn set_buffer(&mut self) {
        if self.buffer_start == self.buffer.len() {
            self.buffer_start = 0;
            self.buffer_end = self.reader.read(&mut self.buffer)?;
        } else if self.buffer_start == self.buffer_end {
            let view = &mut self.buffer[self.buffer_start..];
            let bytes_read = self.reader.read(view)?;
            self.buffer_end += bytes_read;
        }
    }

    #[throws(anyhow::Error)]
    fn set_writer(&mut self) {
        if self.writer.is_none() {
            let part_file = self.create_writer()?;
            self.writer = Some(part_file);
        } else if self.writer.as_ref().unwrap().is_full() {
            self.writer.as_mut().unwrap().close()?;
            self.part_count += 1;
            let part_file = self.create_writer()?;
            self.writer = Some(part_file);
        }
    }

    #[throws(anyhow::Error)]
    fn write_buffer(&mut self) {
        let view = &self.buffer[self.buffer_start..self.buffer_end];
        let writer = self.writer.as_mut().unwrap();

        let bytes_written = writer.write(view)?;
        self.buffer_start += bytes_written;
        self.bytes_remaining -= bytes_written;
    }

    #[throws(anyhow::Error)]
    fn create_writer(&mut self) -> PartFileWriter {
        let name = format!("{}.{}", self.file_name, self.part_count);
        let path = self.parts_dir.join(&name);

        PartFileWriter::new(path, self.part_size)?
    }
}
