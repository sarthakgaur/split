use fehler::throws;
use std::fs::File;
use std::io::{BufReader, BufWriter, Read, Write};
use std::path::{Path, PathBuf};

const BUFFER_SIZE: usize = 8192;

#[derive(PartialEq, Eq)]
enum ReadStatus {
    Ok,
    Stop,
}

#[derive(Debug)]
pub struct Merger {
    file_name: String,
    parent_path: PathBuf,
    part_count: usize,
    reader: Option<BufReader<File>>,
    writer: BufWriter<File>,
    buffer: [u8; BUFFER_SIZE],
    buffer_start: usize,
    buffer_end: usize,
    last_bytes_read: usize,
}

impl Merger {
    #[throws(anyhow::Error)]
    pub fn new(part_file_path: &Path) -> Merger {
        let file_name = Merger::get_file_name(part_file_path);
        let parent_path = part_file_path.parent().unwrap().to_path_buf();
        let part_count = 0;
        let reader = None;
        let file = File::create(&file_name)?;
        let writer = BufWriter::new(file);
        let buffer: [u8; BUFFER_SIZE] = [0; BUFFER_SIZE];
        let buffer_start = 0;
        let buffer_end = 0;
        let last_bytes_read = 0;

        Merger {
            file_name,
            parent_path,
            part_count,
            reader,
            writer,
            buffer,
            buffer_start,
            buffer_end,
            last_bytes_read,
        }
    }

    #[throws(anyhow::Error)]
    pub fn merge(&mut self) {
        loop {
            let status = self.set_reader();

            if status == ReadStatus::Ok {
                self.set_buffer()?;
                self.write_buffer()?;
            } else {
                self.close_writer()?;
                break;
            }
        }
    }

    fn set_reader(&mut self) -> ReadStatus {
        if self.reader.is_none() || self.last_bytes_read == 0 {
            self.reader = self.create_reader();
            self.part_count += 1;
        }

        match self.reader {
            Some(_) => ReadStatus::Ok,
            None => ReadStatus::Stop,
        }
    }

    #[throws(anyhow::Error)]
    fn set_buffer(&mut self) {
        if self.buffer_start == self.buffer.len() {
            let reader = self.reader.as_mut().unwrap();
            let bytes_read = reader.read(&mut self.buffer)?;

            self.last_bytes_read = bytes_read;
            self.buffer_start = 0;
            self.buffer_end = bytes_read;
        } else if self.buffer_start == self.buffer_end {
            let reader = self.reader.as_mut().unwrap();
            let view = &mut self.buffer[self.buffer_start..];
            let bytes_read = reader.read(view)?;

            self.last_bytes_read = bytes_read;
            self.buffer_end += bytes_read;
        }
    }

    #[throws(anyhow::Error)]
    fn write_buffer(&mut self) {
        let view = &self.buffer[self.buffer_start..self.buffer_end];

        let bytes_written = self.writer.write(view)?;
        self.buffer_start += bytes_written;
    }

    fn create_reader(&self) -> Option<BufReader<File>> {
        let part_name = format!("{}.{}", self.file_name, self.part_count);
        let part_path = self.parent_path.join(&part_name);
        let file = File::open(&part_path);

        if let Ok(part_file) = file {
            Some(BufReader::new(part_file))
        } else {
            None
        }
    }

    #[throws(anyhow::Error)]
    pub fn close_writer(&mut self) {
        self.writer.flush()?
    }

    fn get_file_name(part_file_path: &Path) -> String {
        let part_file_name = part_file_path.file_name().unwrap();
        let part_file_name = part_file_name.to_str().unwrap();
        let string_vec: Vec<&str> = part_file_name.split('.').collect();
        let end_index = string_vec.len() - 1;
        let string_vec = &string_vec[..end_index];

        string_vec.join(".")
    }
}
